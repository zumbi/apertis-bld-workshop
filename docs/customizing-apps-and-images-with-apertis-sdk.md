---
title: Customizing apps and images with Apertis SDK
short-description: Bosch Linux Days 2018
authors:
    - name: Héctor Orón Martínez
...

# Apertis overview

Apertis is a embedded Linux distribution, based on open source Debian GNU/Linux distribution, tailored to the automotive needs and fit for a wide variety of electronic devices (providing a rich connectivity framework). Security and modularity are two of its primary strengths. Apertis provides a feature-rich framework for add-on software and resilient upgrade capabilities. Beyond an operating system, it offers new API, tools and cloud services.

## Apertis infrastructure services

Apertis project provides multiple tools and infrastructure services to ease development.

* `Phabricator`: tracks issues, bugs and feature requests
* `Apertis Wiki`: contains guides, specifications, documentation, release notes and other general information
* `Open Build Service (OBS)`: builds Debian packages for different architectures
* `Linaro Automated Validation Architecture (LAVA)`: automates the system testing of Apertis images
* `Apertis Debian package repository`: publishes the Apertis packages in binary and source format
* `Apertis image repository service`: publishes the example Apertis images targeting different use-cases
* `Apertis GitLab`: hosts source code and provides a GitHub-like review workflow
* `Apertis Jenkins service`: orchestates all the the other components to drive different CI/CD stages automatically

# Hands-On: Apertis SDK

Apertis provides a complete environment for development in the form of a VirtualBox virtual machine (VM). This ensures that developers have access to the environment needed regardless of the operating system that they run on their workstation.

Setup VirtualBox via the recommended route for your platform:
* For `Windows and Mac OSX` download from [VirtualBox website](https://www.virtualbox.org/)
* For `Linux` install via your distribution's package management

The [SDK image](https://images.apertis.org/release/18.09/18.09.0/amd64/sdk/apertis_18.09-sdk-amd64-sdk_18.09.0.vdi.gz) needs to be downloaded from the Apertis website.

Instructions are available on the Apertis wiki describing how to boot the SDK image in [VirtualBox](https://wiki.apertis.org/VirtualBox).

## Initial setup

For this training, Apertis has also enabled a repository providing support packages. To enable it, do as follows:
```
$ echo "deb https://repositories.apertis.org/apertis 18.09 demo" \
  | sudo tee -a /etc/apt/sources.list.d/training.list
$ echo "deb-src https://repositories.apertis.org/apertis 18.09 demo" \
  | sudo tee -a /etc/apt/sources.list.d/training.list
$ sudo apt update
```

## Setup terminal and IDE/editor

Apertis SDK image provides development tools for rapid application development. It integrates the Eclipse IDE, however other tools can be installed using the APT package manager with a command line tool in a terminal. For example to install image creation tool `debos` and source code management tool `git`, run the following commands:
```
$ sudo apt update
$ sudo apt install git debos
```

The SDK also provides a minimal CLI text editor (`vi`) installed by default. To install a more feature rich version run:
```
$ sudo apt update
$ sudo apt install vim
```

Alternatively, you might want to install the `nano` command line editor, which is easier to use for people not familiar with `vim`:
```
$ sudo apt install nano
```

If you want a text editor that is lighter than a full IDE, but is not a terminal application, `mousepad` and `geany` are also available.

It is possible to generate a custom SDK image by creating an image recipe for
that, we'll discuss that later.

# Hands-on: Apertis application development

This section will run through the steps expected to be taken by a developer when developing, packaging and adding an application to the Apertis ecosystem. The main topics we will cover are:

* Developing the application in the SDK
* Cross compiling and running the application on the target
* Packaging it as a Debian Package
* Uploading it to Apertis/OBS

We will be using a simple console based "Hello! World" program called `debconsolepkg` during this execise.

## Get application source

Download the example code and extract the source using tar:

```
$ wget https://repositories.apertis.org/apertis/pool/demo/d/debconsolepkg/debconsolepkg_1.0.orig.tar.xz
$ mkdir -p ~/build/debconsolepkg ; cd ~/build/debconsolepkg ;
$ tar -xf ../../debconsolepkg_1.0.orig.tar.xz
```

## Build source using the applications own tools

Now we have the source, first we will build it to ensure that it builds fine.

```
$ mkdir build-x86 ; cd build-x86 ; cmake ../ ; make
```

We now should have the test application compiled, and we can run it from its current location:

```
$ ./CMakeHelloWorld
```

## Cross-compiling for ARM target

Sometimes it may not be possible to properly run an application within the SDK because the application may require specific functionality provided only by the target hardware, or in case bugs may not be reproduced in the SDK, or even if the goal is to evaluate the performance of the application on the target for example. Since application is not yet packaged and built via the build system, we need to cross-build it and upload it to the target to perform its testing.

In order to do an application cross build we need the following:

* Use a working cross-compile toolchain - this is installed by default in the SDK
* Download or assemble a working `sysroot` containing cross-compiled libraries, headers and other build artifacts
* Tune the application build system preferences to use the cross-compile toolchain and the `sysroot`

Let's build a sysroot using `debos` with the following commands:
```
$ git clone https://gitlab.apertis.org/zumbi/apertis-bld-image-recipes
$ cd apertis-bld-image-recipes
$ sudo debos apertis-ospack.yaml
$ sudo debos apertis-sysroot.yaml
$ mkdir -p ~/build/sysroot ; sudo tar xzf apertis-sysroot.tar.gz \
  -C ~/build/sysroot
```
We will use the source that we have already extracted when building in the SDK. In order to cross-compile it, we need to set a number of CMake settings.
We do this by creating a `CMakeToolchain.txt` file:

```
$ cd ~/build/sysroot
$ ./cmake-cross.sh > ~/CMakeToolchain.txt
```

Now we are ready to cross build our example application. Within the source directory trigger the cross build with:

```
$ cd ~/build/debconsolepkg
$ cp ~/CMakeToolchain.txt .
$ mkdir -p build-arm; cd build-arm;
$ cmake -D CMAKE_TOOLCHAIN_FILE=../CMakeToolchain.txt  ../ ; make
```

To verify that the resulting binary has been built for the ARM architecture run:

```
$ file CMakeHelloWorld
CMakeHelloWorld: ELF 32-bit LSB shared object, ARM, EABI5 version 1 (SYSV),
dynamically linked, interpreter /lib/ld-linux-armhf.so.3, for GNU/Linux 3.2.0,
BuildID[sha1]=8050d312686e29af35a91db8c1ff0723fac40a4e, not stripped
```

# Hands-on: Apertis application integration

Once the application is sufficently ready to be exposed to a wider audience, it is time to package it and publish it to the build service so it can be integrated in to the Apertis OS.

## Packaging your application

Apertis operating system is a Debian/Ubuntu derivative so it uses the `deb` packaging format.

A number of items need to be provided for the Debian tools to create a valid `deb` source package:

* `Upstream compressed tarball (.orig.tar.*)`: this should be a release tarball provided by upstream or a tarball of an internal release.
  * The tarball should be included unmodified from the upstream project, assuming that a well behaving tarball is released, the file will need to be renamed to match what is expected by the Debian tooling.
  * The Debian tools can handle tar archives compressed with the gzip (`.gz`), bzip2 (`.bz2`) or xz (`.xz`) compression formats.
* `Debian directory inside uncompressed upstream source`: this contains a `debian` directory with metadata and changes against uptream sources.
  * It contains `patches`, relationships and dependencies with other packages (`control` file) and `rules` required to build the package(s) generated from the source.
  * Scripts used during the instal and removal of generated package(s), also known as maintainer scripts.
  * A `changelog` listing the changes that have been made over the life of this revision of this source archive.

Debian tools use these to build one or more `.deb` files which are the binary packages produced from the source. Some simple source packages may produce only 1 binary package. Other source packages will split the output across multiple binary packages with executables, libraries, documentation and header files split out in there own packages, thus allowing subsets of the generated components to be installed in the running system. Inside upstream source with `debian` directory populated, easiest way to generate source package is:
```
$ dpkg-buildpackage -us -uc
```
(`-us` and `-uc` are meant for unsigned source and unsigned changes. See `man dpkg-buildpackage` for more information.)

Then a set of artifacts will be generated:

* `Debian description (.dsc) file`: this file provides a basic overview of the item being packaged.
  * It provides some basic information about the package.
  * It lists the other items that are used to generate the package(s) and provides checksums so that the provided files can be validated.
* `Debian compressed tarball (.debian.tar.*)`: compressed tarball containing Debian directory metadata.

The main files in the Debian metadata are:
* `debian/control` - meta-data about package (binary packages listing, relationships with other packages at build and runtime)
* `debian/rules` - makefile which specifies how to build a package
* `debian/copyright` - copyright information for the package (see [DEP-5 copyright format](http://dep.debian.net/deps/dep5))
* `debian/changelog` - history of Debian package

For a quick packaging tutorial, see [Debian packaging tutorial](https://www.debian.org/doc/manuals/packaging-tutorial/packaging-tutorial.en.pdf).
For further documentation on packaging, see [Debian developers' manuals](https://www.debian.org/doc/devel-manuals).

## Example to Debianize a source tarball into a deb package using dh_make

We are going to start by getting the source into the directory structure expected by the Debian tools:

* Install the required Debian tools:

```
$ sudo apt update
$ sudo apt install dh-make
```

* Create a folder in the form `<pkgname>-<version>`, for example `debconsolepkg-1.0`
* Copy your entire source code into the folder
* Run the following `dh_make` command from the new folder to begin the initial debianization process:

```
$ cd debconsolepkg-1.0
$ dh_make -s --createorig
Email-Address       : user@unknown
License             : blank
Package Name        : debconsolepkg
Maintainer Name     : User
Version             : 1.0
Package Type        : single
Date                : Fri, 23 Nov 2018 08:52:12 +0000
Are the details correct? [Y/n/q]
Currently there is not top level Makefile. This mayrequire additional tuning
Done. Please edit the files in the `debian` subdirectory now.
```

The arguments to `dh_make` are as follows:
* `-s` switch is used for a single binary application
* `--createorig` switch is used for creating a pristine .orig.tar file for packaging

Notice the newly created `debian` folder. This folder contains all the necessary metadata required for packaging.

```
$ ls
CMakeLists.txt  Hello  HelloWorld.cpp  LICENSE  README.md  debian
```

Modify `debian/control` file to suit your needs. More information about which fields are required and guidance on their usage can be found in the [Debian policy manual](https://www.debian.org/doc/debian-policy/#document-ch-controlfields)

Specifically, in this instance:

* Add '`, cmake`' to the end of `Build-Depends` as the package uses the cmake build system.
* Add `Debian package demo` as the short description (First line of `Description`).
* Add the following as the long description:

```
Longer description of Debian Package Demo
.
Some more description
```

Let's build the package:

```
$ dpkg-buildpackage -uc -us
dpkg-buildpackage: info: source package debconsolepkg
dpkg-buildpackage: info: source version 1.0-1
dpkg-buildpackage: info: source distribution unstable
dpkg-buildpackage: info: source changed by User <user@unknown>
dpkg-source --before-build debconsolepkg-1.0
fakeroot debian/rules clean
dh clean
   dh_clean
dpkg-source -b debconsolepkg-1.0
dpkg-source: info: using source format '3.0 (quilt)'
dpkg-source: info: building debconsolepkg using existing ./debconsolepkg_1.0.orig.tar.xz
dpkg-source: info: building debconsolepkg in debconsolepkg_1.0-1.debian.tar.xz
dpkg-source: info: building debconsolepkg in debconsolepkg_1.0-1.dsc
dpkg-genbuildinfo --build=source
dpkg-genchanges --build=source >../debconsolepkg_1.0-1_source.changes
dpkg-genchanges: info: including full source code in upload
dpkg-source --after-build debconsolepkg-1.0
dpkg-buildpackage: info: full upload (original source is included)
```

Use `dpkg` to look at the contents of the package that has been created:

```
$ cd ..
$ dpkg --contents debconsolepkg_1.0-1_amd64.deb 
drwxr-xr-x root/root         0 2018-11-23 09:26 ./
drwxr-xr-x root/root         0 2018-11-23 09:26 ./usr/
drwxr-xr-x root/root         0 2018-11-23 09:26 ./usr/bin/
-rwxr-xr-x root/root      6352 2018-11-23 09:26 ./usr/bin/CMakeHelloWorld
-rwxr-xr-x root/root      2606 2018-11-23 09:26 ./usr/bin/libHello.a
drwxr-xr-x root/root         0 2018-11-23 09:26 ./usr/include/
-rw-r--r-- root/root       124 2018-11-23 09:32 ./usr/include/Speaker.h
drwxr-xr-x root/root         0 2018-11-23 09:26 ./usr/share/
drwxr-xr-x root/root         0 2018-11-23 09:26 ./usr/share/doc/
drwxr-xr-x root/root         0 2018-11-23 09:26 ./usr/share/doc/debconsolepkg/
-rw-r--r-- root/root       176 2018-11-23 08:53 ./usr/share/doc/debconsolepkg/README.Debian
-rw-r--r-- root/root      1673 2018-11-23 08:53 ./usr/share/doc/debconsolepkg/copyright
-rw-r--r-- root/root       172 2018-11-23 08:53 ./usr/share/doc/debconsolepkg/changelog.Debian.gz
```

Further examples on `dh-make` can be found at the [new maintainers' guide](https://www.debian.org/doc/manuals/maint-guide/first.en.html#dh-make).

## Install and run project

We now have a package generated using the SDK. We can now install this locally:

```
$ sudo apt install ./debconsolepkg_1.0-1_amd64.deb
or
$ sudo dpkg -i debconsolepkg_1.0-1_amd64.deb
```

Note: `apt` will handle extra needed dependencies that need to be satisfied, while `dpkg` only deals with the package itself and it shall error out if missing dependencies are not satisfiable. In summary, `dpkg` knows about package, and `apt` knows about package sets and the dependencies between them.

We can now run the application:

```
$ CMakeHelloWorld
Hello, world!
```

## Adding your application to default images

Images are built using a tool called `debos`, it automates the tasks for creating images, sysroots, ospacks and hardware packs. In order to do this `debos` uses recipes which describe how an image is to be created, such as which packages must be included in the image. These recipes are provided in the form of a `yaml` file description. For more information regarding the format of these files, see [debos actions](https://godoc.org/github.com/go-debos/debos/actions) in the `debos` documentation.

The task of generating images is further automated by having Jenkins run `debos` automatically. This is done daily to provide the "nightly" images. Therefore, if the package after it is uploaded and tested needs to be enabled in the example images a request should be made to modify one or more of the image recipes to add your package.

Example recipes are created by Collabora and those can be found at the `apertis-image-recipes` GitLab source repository.

# Hands-on: Apertis images (ospack)

## Apertis example images

There are different Apertis images: target, sdk, minimal, tiny

* target - GPLv3-free example image with a demo HMI
* sdk - reference image to run on VirtualBox with the XFCE desktop system and development tools, IDEs, cross compilers and everything needed for rapid application development 
* minimal - an headless image providing a good basis for product-specific customizations
* tiny - a very minimal environment tuned to run as LXC container

Images are built from an ospack (hardware, but not architecture, independent software) and hwpack (hardware dependent software). For those `debos` image recipes are created.

## Apertis adding features (ospack)

When creating system images, Apertis splits hardware independent software in operative system pack (ospack), which effectively is a root filesystem that contains unpacked packages. The ospacks are generated using `debos`.

A basic ospack is generated from the `apertis-image-recipes` git repository, see `apertis-ospack.yaml`. Modifications like adding new repositories, running certain commands or installing extra packages can be done describing them in the `yaml` file, i.e. a package can be added to the ospack using an `apt` action block as described in [debos documentation](https://godoc.org/github.com/go-debos/debos/actions). More than one package can be installed in each block by listing them (one per line), in the `packages` section. The following block would add `debconsolepkg` to an image:

```
  - action: apt
    description: "Demo example"
    packages:
      - debconsolepkg
```

Newer blocks can be created to add different software stacks, describing why those are needed.

After we have defined our ospack recipe, it is time to build it:

```
$ sudo debos apertis-ospack.yaml
```

`debos` usually uses virtualization technology to avoid admin rights, however this currently cannot be done from within the VirtualBox environment, so we are using `debos` without virtualization in SDK.

Automating ospack builds must be done using Jenkins service.

# Hands-on: Apertis images (hwpack)

## Apertis hardware enablement (hwpack)

Enabling new hardware essentially consists in adding hardware specific packages to the image build system, for those we need to create packages for such components, mainly:

* Bootloader
* Linux kernel
* Graphic stack

Since we are not using any hardware acceleration in this workshop, the graphic stack will not be under discussion in this section as no hardware specific components are required. The hwpack for the target hardware contains the bootloader (`u-boot`) and the Linux kernel (`linux-image-armmp`).

Just as with other packages we need to provide a copy of the original source and the Debian metadata, which contains the Debian specific configuration and modifications. As with early development of other packages, packaging tends to be done once some initial level of functionality has been obtained. It is advised to ensure that any modifications are tracked and correctly split into well described commits/patches as this will aid with future porting, bugfixing and/or upstreaming exercises.

Add `u-boot-omap` bootloader and `linux-image-armmp` kernel packages to the `debos` yaml recipe along any further tweaks to install bootloader.

```
  - action: apt
    description: Install hardware support packages
    packages:
      - linux-image-armmp
      - u-boot-omap

  - action: raw
    description: Flash MLO
    offset: {{ sector 256 }}
    origin: filesystem
    source: /usr/lib/u-boot/am335x_boneblack/MLO

  - action: raw
    description: Flash u-boot
    origin: filesystem
    offset: {{ sector 768 }}
    source: /usr/lib/u-boot/am335x_boneblack/u-boot.img
```

## Building images

The images containing the hwpack items are built by `debos`.
```
$ sudo debos apertis-image.yaml
```
An image shall be created in the home directory, that image can be used in the target hardware or taken for validation under LAVA service.

LAVA service is part of the continuous integration system and it is the software framework that runs tests over system images to validate and verify them making sure no regressions are introduced in the critical features.
