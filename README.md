# Introduction

This repository contains workshop documents written by Collabora
for the Bosch Linux Days 2018 on the topic of Apertis platform.

# Licensing

This workshop is licensed under CC BY-SA 4.0 International. See COPYING
for more details.

# Build documentation

Documentation builds using [hotdoc](https://github.com/hotdoc/hotdoc) tool, to be able to get proper
environment, a docker image exists to build documentation as follows:

```
RELEASE_VERSION="18.09" ;
docker run --device /dev/kvm \
    -w /documentation \
    -u $(id -u) \
    --group-add=$(getent group kvm | cut -d : -f 3) \
    -i -v $(pwd):/documentation \
    -t docker-registry.apertis.org/apertis/apertis-${RELEASE_VERSION}-documentation-builder \
    make
```
If you want PDF version use `make pdf` instead.
